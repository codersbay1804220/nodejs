const mysql = require('mysql');

const con = mysql.createConnection({
    host: "localhost",
    port: 55000,
    user: "root",
    password: "root"
});

con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
    con.query("CREATE DATABASE test_db", function (err, result) {
        if (err) throw err;
        console.log("Database created");
    });
});
