const mysql = require('mysql');

const con = mysql.createConnection({
    host: "localhost",
    port: 55000,
    user: "root",
    password: "root",
    database: 'test_db'
});

con.connect((err)  => {
    if (err) throw err;
    console.log("Connected!");
    const sql = "INSERT INTO customers (name, address) VALUES ('Company Inc 2', 'Highway 33')";
    con.query(sql,  (err, result) => {
        if (err) throw err;
        console.log("1 record inserted");
    });
});
