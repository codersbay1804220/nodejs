const fs = require('fs');

// read file asynchronously
fs.readFile('TestFile.txt',  'utf8', (err, data) => {
    if (err) {
        throw err;
    }

    console.log(data);
});

// read file synchronously
const data = fs.readFileSync('TestFile2.txt', 'utf8');
console.log(data);

// write file
fs.writeFile('test.txt', 'Hello World!', function (err) {
    if (err) {
        console.log(err);
    } else {
        console.log('Write operation complete.');
    }
});

//Append File
fs.appendFile('test2.txt', 'Hello World!', function (err) {
    if (err) {
        console.log(err);
    } else {
        console.log('Append operation complete.');
    }
});

//Delete File
fs.unlink('test.txt', function (err) {
    if (err) throw err;
    console.log('File deleted!');
});
