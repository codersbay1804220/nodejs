const express = require('express')
const app = express()

const port = 3000
const hostname = 'localhost'

app.use((err, req, res, next) => {
    console.error(err.stack)
    res.status(500).send('Something broke!')
})
app.get('/', (req, res) => {
    throw new Error('Something went wrong')
    res.send('Hallo Welt')
})

app.listen(port, hostname, () => {
    console.log('Server run on http://localhost:3000')
})



//
// // GET method route
// app.get('/', (req, res) => {
//     res.send('GET request to the homepage')
// })
//
// app.route('/book')
//     .get((req, res) => {
//         res.send('Get a random book')
//     })
//     .post((req, res) => {
//         res.send('Add a book')
//     })
//     .put((req, res) => {
//         res.send('Update the book')
//     })
