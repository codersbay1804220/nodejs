const express = require('express')
const bodyParser = require('body-parser')

const app = express()
const port = 3000
const hostname = 'localhost'

// create application/json parser
//app.use(express.json())
app.use(bodyParser.json())

app.listen(port, hostname, () => {
    console.log('Server run on http://localhost:3000')
})

app.get('/', (req, res) => {
    console.log(req)
    res.send('Hallo Welt')
})

app.post('/test', (req, res) => {
    console.log(req.body)
    res.send('Hallo Welt')
})

app.patch('/test/:id', (req, res) => {
    console.log(req.params.id)
    res.send('Hallo Welt')
})
