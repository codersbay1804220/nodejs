const express = require('express');
const app = express();

const logger = function (req, res, next) {
    console.log('LOGGED');
    next();
};

app.use(logger);

app.get('/', function (req, res) {
    res.send('Hello World!');
});

app.listen(3000);
