const LocalStrategy = require('passport-local').Strategy
const database = require('../services/database')

const options = {usernameField: 'email'}
const strategy = new LocalStrategy(options, async (email, password, done) => {
    try {
        const user = await database.findUserByCredentials({email, password})
        done(null, user) // setz den user gleich in req.user
    } catch (e) {
        done(e)
    }
})

module.exports = strategy
