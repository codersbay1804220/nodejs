const mysql = require('mysql2/promise')
const bcrypt = require('bcryptjs')

let connection
mysql.createConnection({
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: 'root',
    database: 'auth_db'
}).then((con) => {
    connection = con
    console.log('Database connected')
}).catch((err) => {
    console.log('Error: Database connection failed')
})

async function getUsers() {
    const [result] = await connection.execute('SELECT * FROM user')

    return result
}

async function createUser({name, password, username, email}) {
    const hash = await bcrypt.hash(password, 10)
    await connection.execute('INSERT INTO user (name, username, email, password) VALUES (?, ? ,? ,?)', [name, username, email, hash])
}

async function findUserByCredentials({email, password}){
    const [result] = await connection.execute('SELECT * FROM user WHERE email = ? LIMIT 1', [email])
    if (!result.length) {
      throw new Error('User not found')
    }

    const user = result[0]

    const match = await bcrypt.compare(password, user.password)
    if (!match) {
        throw new Error('Password not match')
    }

    delete user.password
    return user
}

module.exports = {
    getUsers,
    createUser,
    findUserByCredentials
}
