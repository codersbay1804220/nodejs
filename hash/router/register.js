const express = require('express')
const router = express.Router()
const database = require('../services/database')

router.post('/register', async (req, res) => {
    await database.createUser(req.body)
    res.json(req.body)
})

module.exports = router
