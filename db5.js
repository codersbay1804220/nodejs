const mysql = require('mysql');
const http = require('http');

const con = mysql.createConnection({
    host: "localhost",
    port: 55000,
    user: "root",
    password: "root",
    database: 'test_db'
});

con.connect();

const server = http.createServer( (req, res) => {
    con.query("SELECT * FROM customers",  (err, result, fields) =>  {
        if (err) throw err;
        res.writeHead(200, {'Content-Type': 'text/html'});
        for (let r of result) {
            res.write("Name: " + r.name);
            res.write("<br>")
            res.write("Address: " + r.address);
            res.write("<hr>")
        }
        res.end();
    });
});

server.listen(3000, () => {
    console.log(`Server running at http://localhost:3000/`);
});
