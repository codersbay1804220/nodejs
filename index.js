const http = require('http') // Import Node.js core http module
const hostname = '10.211.4.224' //Define Host to Listen
const port = 3000 // Define Port to Listen

const server = http.createServer((req, res)  => {
    if (req.url === '/') { //check the URL of the current request
        // set response header
        res.writeHead(200, { 'Content-Type': 'text/html' });

        // set response content
        res.write('<html><body><p>Das ist meine Homepage.</p></body></html>');
        res.end();

    } else if (req.url === "/student") {

        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.write('<html><body><p>This is student Robert.</p></body></html>');
        res.end();

    } else if (req.url === "/admin") {

        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write('<html><body><p>This is admin Page.</p></body></html>');
        res.end();
    } else if (req.url === '/api') { //check the URL of the current request
         const myObject = { message: "Ich bin eine JSON Message"}
            res.writeHead(200, { 'Content-Type': 'application/json' });
            res.write(JSON.stringify(myObject));
            res.end();

    }  else {
        res.end('Invalid Request!');
    }
})

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
})
