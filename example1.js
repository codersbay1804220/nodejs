const http = require('http'); // Import Node.js core module
const hostname = '127.0.0.1'; // Define Host to Listen
const port = 3000; // Define Port to Listen

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello World');
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});
