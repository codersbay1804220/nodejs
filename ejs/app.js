const express = require('express')
const path = require("path");
const app = express()
const user = require('./router/user')

app.set('views', './views'); //app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use('/user', user)

app.listen(3000)
