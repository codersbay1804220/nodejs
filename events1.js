const events = require('events');
const eventEmitter = new events.EventEmitter();

//Create an event handler:
const myEvent = function () {
    console.log('I hear a scream!');
}

//Assign the event handler to an event:
eventEmitter.on('scream', myEvent);

//Fire the 'scream' event:
eventEmitter.emit('scream');
