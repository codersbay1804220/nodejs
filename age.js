exports.calcBirthYear = function (age) {
    let currentYear = new Date().getFullYear();
    return currentYear - age;
}

exports.calcAge = function (year) {
    let currentYear = new Date().getFullYear();
    return currentYear - year;
}
