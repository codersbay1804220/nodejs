const events = require('events');
const fs = require("fs");
const eventEmitter = new events.EventEmitter();

// read file asynchronously
fs.readFile('TestFile.txt',  'utf8', (err, data) => {
    if (err) {
        throw err;
    }

    eventEmitter.emit('read', data);
});

//Create an event handler:
const myEvent = function (data) {
    console.log(data);
}

//Assign the event handler to an event:
eventEmitter.on('read', myEvent);


