const http = require('http')
const db = require('mysql')
const url = require('url')

const con = db.createConnection({
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: 'root',
    database: 'test_db'
})

con.connect((err) => {
    if (err) console.log('Connection to database failed')
})

const server
    = http.createServer((req,res) => {

    res.setHeader('Content-Type', 'application/json');
    //api/user bzw. /api/user?id=1 bze .etc.....
    let urlParsed = url.parse(req.url, true)
    console.log(urlParsed)

    // Alle meine GET Requests
    if (req.method === 'GET' && urlParsed.pathname === '/api/user') {

        // /api/user --> weil hier gibt es keinen Query Parameter id, dewegen ist der undefined
        if (urlParsed.query.id === undefined) {
            con.query('SELECT * FROM user', (err, result) => {
                res.end(JSON.stringify(result))
            })
        } else {
            //api/user?id=1
            con.query('SELECT * FROM user WHERE id='+parseInt(urlParsed.query.id), (err, result) => {
                console.log(err)
                res.end(JSON.stringify(result[0])) //Wir wollen nur den ersten listen eintrag zurück bekommen
            })
        }
    } else if (req.method === 'POST' && urlParsed.pathname === '/api/user') {
        let body = ''
        req.on('data', (data) => {
            body += data
        })
        req.on('end', () => {
            let json = JSON.parse(body) // hier wird der JSON String als Object geparsed

            const query = `INSERT INTO user (username, firstname, lastname, email, address) 
VALUES ("${json.username}", "${json.firstname}", "${json.lastname}", "${json.email}", "${json.address}")`;
            console.log(query)

            con.query(query, (err, result) => {
                if (err) throw err;
                console.log("1 record inserted");
                res.statusCode = 201
                res.end(body)
            });
        })
    //DELETE api/user?id=1
    } else if (req.method === 'DELETE' && urlParsed.pathname === '/api/user') {
        const query = `DELETE FROM user WHERE id=${parseInt(urlParsed.query.id)}`
        console .log(query)

        con.query(query, (err, result) => {
            res.statusCode = 204
            res.end()
        })
    } else if (req.method === 'PUT' && urlParsed.pathname === '/api/user') {
        let body = ''
        req.on('data', (data) => {
            body += data
        })
        req.on('end', () => {
            let json = JSON.parse(body) // hier wird der JSON String als Object geparsed

            const query = `UPDATE user SET username="${json.username}", firstname="${json.firstname}", lastname="${json.lastname}",
email="${json.email}", address="${json.address}" WHERE id=${parseInt(urlParsed.query.id)}`;
            console.log(query)

            con.query(query, (err, result) => {
                if (err) throw err;
                console.log("1 record updated");
                res.statusCode = 200
                res.end(body)
            });
        })
    }


})

server.listen(3000, () => {
    console.log('Server run on http://localhost:3000')
})
