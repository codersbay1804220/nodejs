const express = require('express')
const app = express()

const logger = function (req, res, next){
    console.log('LOGGED')
    next()
}

app.use(logger)

app.get('/', (req, res) => {
    res.send('hallo welt')
})

app.get('/home', (req, res) => {
    res.send('hallo Home')
})

app.get('/foo', (req, res) => {
    res.send('hallo Foll')
})

app.listen(3000, () => {
    console.log('Express APP run on http://localhost:3000')
})
