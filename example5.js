const http = require('http'); // Import Node.js core module
const fs = require('fs');

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html');
    fs.readFile('helloworld.html', (err, data) => {
        res.end(data);
    });
});

server.listen(3000, () => {
    console.log(`Server running at http://localhost:3000/`);
});
