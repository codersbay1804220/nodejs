const express = require('express')
const app = express()

app.use(express.json())

const user = require('./router/user')
const post = require('./router/post')

app.use('/api', user)
app.use('/api', post)


app.listen(3000)
