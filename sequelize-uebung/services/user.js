const User = require('../models/user')


// Benutzer erstellen
async function createUser({name, email}) {
    const user = await User.create({name: name, email: email});
    if (user) {
        return user
    }

    throw Error('Cannot create user')
}

// Benutzer updaten
async function updateUserById(id, {name, email}) {
    const update = await User.update({name: name, email: email}, {where: {id: id}})

    if (update) {
        const user = await getUserById(id)
        if (user) {
            return user
        }
    }

    throw Error('Cannot update user')
}

// Benutzer mit bestimmter ID
async function getUserById(id) {
    const user = await User.findByPk(id)

    if (user) {
        return user
    }

    throw Error('Cannot find user')
}

// Benutzer mit bestimmter ID löschen
async function deleteUserById(id) {
    const user = await getUserById(id)

    if (user) {
        return await user.destroy()
    }

    throw Error('Cannot delete user')
}

// Alle Benutzer auflisten
async function getAllUser() {
    return await User.findAll()
}

module.exports = {
    getUserById,
    getAllUser,
    deleteUserById,
    updateUserById,
    createUser
}

