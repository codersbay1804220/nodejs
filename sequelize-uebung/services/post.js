const Post = require('../models/post')
const User = require('../models/user')


// Post erstellen
async function createPost({title, content, userId}) {
    const post = await Post.create({title: title, content: content, userId: userId}, {include: User});
    if (post) {
        return post
    }

    throw Error('Cannot create post')
}

// Post updaten
async function updatePostById(id, {title, content, userId}) {
    const update = await Post.update({title: title, content: content, userId: userId}, {where: {id: id}})

    if (update) {
        const post = await getPostById(id, {include: User})
        if (post) {
            return post
        }
    }

    throw Error('Cannot update post')
}

// Post bestimmter ID
async function getPostById(id) {
    const post = await Post.findByPk(id, {include: User})

    if (post) {
        return post
    }

    throw Error('Cannot find post')
}

// Post mit bestimmter ID löschen
async function deletePostById(id) {
    const post = await getPostById(id)

    if (post) {
        return await post.destroy()
    }

    throw Error('Cannot delete post')
}

// Alle Post auflisten
async function getAllPost() {
    return await Post.findAll({include: User})
}

module.exports = {
    getPostById,
    getAllPost,
    deletePostById,
    updatePostById,
    createPost
}

