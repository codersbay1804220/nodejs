const Sequelize = require('sequelize');
const sequelize = require('../services/sequelize');
const User = require('./user');

// Modell für Post erstellen
const Post = sequelize.define('post', {
    title: {
        type: Sequelize.STRING,
        allowNull: false
    },
    content: {
        type: Sequelize.STRING,
        allowNull: false,
    }
}, {
    timestamps: false,
    freezeTableName: true
});

// Beziehungen zwischen Benutzern und Beiträgen definieren
User.hasMany(Post);
Post.belongsTo(User);

module.exports = Post;
