const Sequelize = require('sequelize');
const sequelize = require('../services/sequelize');

// Modell für Benutzer erstellen
const User = sequelize.define('user', {
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    }
}, {
    timestamps: false,
    freezeTableName: true
});

module.exports = User;
