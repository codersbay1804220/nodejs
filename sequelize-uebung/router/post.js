const express = require ('express')
const router = express.Router()
const Post = require('../services/post')

router.get('/post', async (req, res) => {
    try {
        const users = await Post.getAllPost()
        res.json(users)
    } catch (e) {
        res.status(400).send(e)
    }
})

router.get('/post/:id', async (req, res) => {
    try {
        const user = await Post.getPostById(req.params.id)
        res.json(user)
    } catch (e) {
        res.status(400).send(e)
    }
})

router.post('/post', async (req, res) => {
    try {
        const user = await Post.createPost(req.body)
        res.status(201).json(user)
    } catch (e) {
        res.status(400).send(e)
    }
})

router.put('/post/:id', async (req, res) => {
    try {
        const user = await Post.updatePostById(req.params.id, req.body)
        res.json(user)
    } catch (e) {
        res.status(400).send(e)
    }
})

router.delete('/post/:id', async (req, res) => {
    try {
        const user = await Post.deletePostById(req.params.id)
        res.status(204).json()
    } catch (e) {
        res.status(400).send(e)
    }
})



module.exports = router
