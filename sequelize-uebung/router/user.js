const express = require ('express')
const router = express.Router()
const User = require('../services/user')

router.get('/user', async (req, res) => {
    try {
        const users = await User.getAllUser()
        res.json(users)
    } catch (e) {
        res.status(400).send(e)
    }
})

router.get('/user/:id', async (req, res) => {
    try {
        const user = await User.getUserById(req.params.id)
        res.json(user)
    } catch (e) {
        res.status(400).send(e)
    }
})

router.post('/user', async (req, res) => {
    try {
        const user = await User.createUser(req.body)
        res.status(201).json(user)
    } catch (e) {
        res.status(400).send(e)
    }
})

router.put('/user/:id', async (req, res) => {
    try {
        const user = await User.updateUserById(req.params.id, req.body)
        res.json(user)
    } catch (e) {
        res.status(400).send(e)
    }
})

router.delete('/user/:id', async (req, res) => {
    try {
        const user = await User.deleteUserById(req.params.id)
        res.status(204).json()
    } catch (e) {
        res.status(400).send(e)
    }
})



module.exports = router
