const ageYear
    = require('./age')

// calc birthYear by given age
let birthYear = ageYear.calcBirthYear(36);
console.log(birthYear)

// calc age by given year
let age = ageYear.calcAge(1987);
console.log(age)
