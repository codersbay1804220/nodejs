const mysql = require('mysql');

const con = mysql.createConnection({
    host: "localhost",
    port: 55000,
    user: "root",
    password: "root",
    database: 'test_db'
});

con.connect((err) => {
    if (err) throw err;
    con.query("SELECT * FROM customers",  (err, result, fields) =>  {
        if (err) throw err;
        console.log(result);
    });
})
