const http = require('http');

const server = http.createServer((req, res) =>{

    if (req.url === '/data') { //check the URL of the current request
        res.writeHead(200, { 'Content-Type': 'application/json' });
        res.write(JSON.stringify({ message: "Hello World"}));
        res.end();
    }  else {
        res.end('Invalid Request!');
    }
});

server.listen(3000, () => {
    console.log(`Server running at http://localhost:3000/`);
});
