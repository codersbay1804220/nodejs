const express = require('express')
const router = express.Router()

const db = require('mysql')

const con = db.createConnection({
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: 'root',
    database: 'test_db'
})

con.connect((err) => {
    if (err) console.log('Connection to database failed')
})

const headerMW = function (req, res, next) {
    //res.set('Content-Type', 'application/json')

    next()
}



router.use(express.json())
router.use(headerMW)

// GET /api/user
router.get('', (req, res) => {
    con.query('SELECT * FROM user', (err, result) =>  {
        res.json(result)
    })
})

// GET /api/user/{UserID}
router.get('/:id', async (req, res) => {
    await con.query(`SELECT * FROM user WHERE id=${req.params.id}`, (err, result) => {
        res.send(JSON.stringify(result[0]))
    })
})

// POST /api/user
router.post('', (req,res) => {
    console.log(req.body)
    con.query(`INSERT INTO user (username, firstname, lastname, email, address) 
VALUES ("${req.body.username}","${req.body.firstname}", "${req.body.lastname}", "${req.body.email}", "${req.body.address}")`, (err, result) => {
        res.statusCode = 201
        res.send(JSON.stringify(req.body))
    })
})

// DELETE /api/user/{UserID}
router.delete('/:id', (req, res) => {
    con.query(`DELETE FROM user WHERE id=${parseInt(req.params.id)}`, (err, result) => {
        res.status(204).send('')
    })
})

// PUT /api/user/{UserID}
router.put('/:id', (req, res) => {
    con.query(`
UPDATE user SET 
    username="${req.body.username}", 
    firstname="${req.body.firstname}", 
    lastname="${req.body.lastname}",
    email="${req.body.email}", 
    address="${req.body.address}" 
WHERE id=${parseInt(req.params.id)}`, (err, result) => {
        res.status(200).send(JSON.stringify(req.body))
    })
})

module.exports = router
