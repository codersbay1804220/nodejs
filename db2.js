const mysql = require('mysql');

const con = mysql.createConnection({
    host: "localhost",
    port: 55000,
    user: "root",
    password: "root",
    database: 'test_db'
});

con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
    const sql = "CREATE TABLE customers (name VARCHAR(255), address VARCHAR(255))";
    con.query(sql, function (err, result) {
        if (err) throw err;
        console.log("Table created");
    });
});
